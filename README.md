# CoD4 Spectating Rework

## Brief description
Entity number of the player that's being spectated is saved in spectator's `spectating` variable.

### Disclaimer
At the moment it works like if the cvar `scr_game_spectatetype` was set to `2` (but it's easy to adapt the
script to respect that cvar).


## Problems
1. The part I don't like at all is the code related to blocking rapid switch of spected players when holding
attack or aim button. That's definitely the first thing to rethink and refactor at the moment.
2. The `spectating` variable won't be accurate when spectated player leaves the game or goes to spectate.

## License
The MIT License

Copyright (c) 2016 Rafał Florczak [andr]

You can find more information in the LICENSE file.

## Contact
* Email: ![email.png](https://bitbucket.org/repo/Mo559r/images/3843802376-email.png)
* Website: http://forums.ms-gaming.com